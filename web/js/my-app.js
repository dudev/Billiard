// Initialize your app
var app = new Framework7({
    //pushState: true,
    swipePanel: 'left',
    preroute: function (view, options) {
        if (!userLoggedIn) {
            //view.router.loadPage('login-screen.html'); //load another page with auth form
            //return false; //required to prevent default router action
        }
    }
});

var userLoggedIn = false;

// Export selectors engine
var $ = Framework7.$;

// Add view
var mainView = app.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true,
    url: "training.html"
});
mainView.router.refreshPage();

function checkNewMessages() {
    $(".navbar").removeClass("new-message");
    $.get("/api/im/has-new",
        function(data){
            data = JSON.parse(data);
            if(data.result == "success" && data.has_new) {
                $(".navbar").addClass("new-message");
            }
        }
    );
}
setInterval(checkNewMessages, 5000);

app.onPageInit('training', function (page) {
    function calcBoard() {
        var table_wrap = $(".table-wrap");
        var table = $(".table");
        var table_side = $(".table-side");
        var board = $(".board");

        var table_wrap_width = table_wrap.width();
        var diameter_ball = Math.round(table_wrap_width / 51);
        var table_width = table_wrap_width - 2 * diameter_ball;

        table_wrap.css({
            height: (table_wrap_width / 2) + "px",
            "margin-bottom": diameter_ball + "px"
        });

        table.css({
            margin: "0 " + diameter_ball + "px",
            height: Math.round(table_width / 2) + "px",
            top: diameter_ball + "px",
            width: table_width + "px"
        });

        board.css({
            top: (diameter_ball / 2 + 0.5) + "px",
            margin: "0 " + (diameter_ball / 2) + "px",
            height: Math.round(table_width / 2 - (diameter_ball + 1)) + "px",
            width: (table_width - (diameter_ball + 1)) + "px"
        });

        table_side.css({
            height: (table_width / 2) + "px",
            width: (table_wrap_width - 2 * diameter_ball) + "px",
            "border-width": diameter_ball + "px"
        });

        (function(diameter_ball) {
            var left = $(".boardside-top .left");
            var center = $(".boardside-top .center");
            var right = $(".boardside-top .right");
            var shadow = $(".boardside-top .shadow");
            var right_part = $(".boardside-top .right-part");
            left.css({
                top: (-Math.round(diameter_ball / Math.sqrt(2))) + "px",
                "border-width": Math.round(diameter_ball / Math.sqrt(2)) + "px",
                left: (diameter_ball * 2 + 2 - diameter_ball / 2) + "px",
                "box-shadow": "0 0 " + Math.round(diameter_ball * 0.625) + "px " + Math.round(diameter_ball < 4 ? 1 : diameter_ball * 0.125) + "px rgba(0, 0, 0, 0.9)"
            });
            right.css({
                top: (-Math.round(diameter_ball / Math.sqrt(2))) + "px",
                "border-width": Math.round(diameter_ball / Math.sqrt(2)) + "px",
                left: Math.round(table.width() / 2 - Math.round(diameter_ball / Math.sqrt(2) + diameter_ball + 2) - diameter_ball) + "px",
                "box-shadow": "0 0 " + Math.round(diameter_ball * 0.625) + "px " + Math.round(diameter_ball < 4 ? 1 : diameter_ball * 0.125) + "px rgba(0, 0, 0, 0.9)"
            });
            center.css({
                height: diameter_ball + "px",
                left: Math.round(diameter_ball / Math.sqrt(2) + diameter_ball * 2 + 2 - diameter_ball / 2) + "px",
                width: Math.round(table.width() / 2 - Math.round(diameter_ball / Math.sqrt(2) + diameter_ball + 2) - diameter_ball * 3 - 3 + diameter_ball / 2) + "px"
            });
            shadow.css({
                top: diameter_ball + "px",
                left: Math.round(diameter_ball / Math.sqrt(2) + diameter_ball * 2 + 2 - diameter_ball / 2) + "px",
                width: Math.round(table.width() / 2 - Math.round(diameter_ball / Math.sqrt(2) + diameter_ball + 2) - diameter_ball * 3 - 3) + "px",
                "box-shadow": "0 0 " + Math.round(diameter_ball * 0.625) + "px " + Math.round(diameter_ball < 4 ? 1 : diameter_ball * 0.125) + "px rgba(0, 0, 0, 0.9)"
            });
            right_part.css({
                left: Math.round(table.width() / 2 - Math.round(diameter_ball / Math.sqrt(2) + diameter_ball + 1) + diameter_ball + diameter_ball / 2) + "px"
            });

            var boardside_bottom = $(".boardside-bottom");
            boardside_bottom.css({
                top: (table.height() - diameter_ball) + "px"
            });
            var left = $(".boardside-bottom .left");
            var center = $(".boardside-bottom .center");
            var right = $(".boardside-bottom .right");
            var shadow = $(".boardside-bottom .shadow");
            var right_part = $(".boardside-bottom .right-part");
            left.css({
                top: Math.round(diameter_ball / 4) + "px",
                "border-width": Math.round(diameter_ball / Math.sqrt(2)) + "px",
                left: (diameter_ball * 2 + 2 - diameter_ball / 2) + "px",
                "box-shadow": "0 0 " + Math.round(diameter_ball * 0.625) + "px " + Math.round(diameter_ball < 4 ? 1 : diameter_ball * 0.125) + "px rgba(0, 0, 0, 0.9)"
            });
            right.css({
                top: Math.round(diameter_ball / 4) + "px",
                "border-width": Math.round(diameter_ball / Math.sqrt(2)) + "px",
                left: Math.round(table.width() / 2 - Math.round(diameter_ball / Math.sqrt(2) + diameter_ball + 2) - diameter_ball) + "px",
                "box-shadow": "0 0 " + Math.round(diameter_ball * 0.625) + "px " + Math.round(diameter_ball < 4 ? 1 : diameter_ball * 0.125) + "px rgba(0, 0, 0, 0.9)"
            });
            center.css({
                height: diameter_ball + "px",
                left: Math.round(diameter_ball / Math.sqrt(2) + diameter_ball * 2 + 2 - diameter_ball / 2) + "px",
                width: Math.round(table.width() / 2 - Math.round(diameter_ball / Math.sqrt(2) + diameter_ball + 2) - diameter_ball * 3 - 3 + diameter_ball / 2) + "px"
            });
            shadow.css({
                left: Math.round(diameter_ball / Math.sqrt(2) + diameter_ball * 2 + 2 - diameter_ball / 2) + "px",
                width: Math.round(table.width() / 2 - Math.round(diameter_ball / Math.sqrt(2) + diameter_ball + 2) - diameter_ball * 3 - 3) + "px",
                "box-shadow": "0 0 " + Math.round(diameter_ball * 0.625) + "px " + Math.round(diameter_ball < 4 ? 1 : diameter_ball * 0.125) + "px rgba(0, 0, 0, 0.9)"
            });
            right_part.css({
                left: Math.round(table.width() / 2 - Math.round(diameter_ball / Math.sqrt(2) + diameter_ball + 1) + diameter_ball + diameter_ball / 2) + "px"
            });

            var left = $(".boardside-left .left");
            var center = $(".boardside-left .center");
            var right = $(".boardside-left .right");
            var shadow = $(".boardside-left .shadow");
            left.css({
                left: (-Math.round(diameter_ball / Math.sqrt(2))) + "px",
                "border-width": Math.round(diameter_ball / Math.sqrt(2)) + "px",
                top: (diameter_ball * 2 + 2 - diameter_ball / 2) + "px",
                "box-shadow": "0 0 " + Math.round(diameter_ball * 0.625) + "px " + Math.round(diameter_ball < 4 ? 1 : diameter_ball * 0.125) + "px rgba(0, 0, 0, 0.9)"
            });
            right.css({
                left: (-Math.round(diameter_ball / Math.sqrt(2))) + "px",
                top: Math.round(table.height()  - 2 * (diameter_ball + 1) - 2 * diameter_ball / Math.sqrt(2) + diameter_ball / 2) + "px",
                "border-width": Math.round(diameter_ball / Math.sqrt(2)) + "px",
                "box-shadow": "0 0 " + Math.round(diameter_ball * 0.625) + "px " + Math.round(diameter_ball < 4 ? 1 : diameter_ball * 0.125) + "px rgba(0, 0, 0, 0.9)"
            });
            center.css({
                height: Math.round(table.height() - 2 * (diameter_ball + 1) - 2 * diameter_ball / Math.sqrt(2) - diameter_ball * 2 - 2 + diameter_ball) + "px",
                top: Math.round(diameter_ball / Math.sqrt(2) + diameter_ball * 2 + 2 - diameter_ball / 2) + "px",
                width: diameter_ball + "px"
            });
            shadow.css({
                left: diameter_ball + "px",
                height: Math.round(table.height() - 2 * (diameter_ball + 1) - 2 * diameter_ball / Math.sqrt(2) - diameter_ball * 2 - 2 + diameter_ball) + "px",
                top: Math.round(diameter_ball / Math.sqrt(2) + diameter_ball * 2 + 2 - diameter_ball / 2) + "px",
                "box-shadow": "0 0 " + Math.round(diameter_ball * 0.625) + "px " + Math.round(diameter_ball < 4 ? 1 : diameter_ball * 0.125) + "px rgba(0, 0, 0, 0.9)"
            });

            var boardside_right = $(".boardside-right");
            boardside_right.css({
                left: (table.width() - diameter_ball) + "px"
            });
            var left = $(".boardside-right .left");
            var center = $(".boardside-right .center");
            var right = $(".boardside-right .right");
            var shadow = $(".boardside-right .shadow");
            left.css({
                left: (diameter_ball / 4) + "px",
                "border-width": Math.round(diameter_ball / Math.sqrt(2)) + "px",
                top: (diameter_ball * 2 + 2 - diameter_ball / 2) + "px",
                "box-shadow": "0 0 " + Math.round(diameter_ball * 0.625) + "px " + Math.round(diameter_ball < 4 ? 1 : diameter_ball * 0.125) + "px rgba(0, 0, 0, 0.9)"
            });
            right.css({
                left: (diameter_ball / 4) + "px",
                top: Math.round(table.height()  - 2 * (diameter_ball + 1) - 2 * diameter_ball / Math.sqrt(2) + diameter_ball / 2) + "px",
                "border-width": Math.round(diameter_ball / Math.sqrt(2)) + "px",
                "box-shadow": "0 0 " + Math.round(diameter_ball * 0.625) + "px " + Math.round(diameter_ball < 4 ? 1 : diameter_ball * 0.125) + "px rgba(0, 0, 0, 0.9)"
            });
            center.css({
                height: Math.round(table.height() - 2 * (diameter_ball + 1) - 2 * diameter_ball / Math.sqrt(2) - diameter_ball * 2 - 2 + diameter_ball) + "px",
                top: Math.round(diameter_ball / Math.sqrt(2) + diameter_ball * 2 + 2 - diameter_ball / 2) + "px",
                width: diameter_ball + "px"
            });
            shadow.css({
                height: Math.round(table.height() - 2 * (diameter_ball + 1) - 2 * diameter_ball / Math.sqrt(2) - diameter_ball * 2 - 2 + diameter_ball) + "px",
                top: Math.round(diameter_ball / Math.sqrt(2) + diameter_ball * 2 + 2 - diameter_ball / 2) + "px",
                "box-shadow": "0 0 " + Math.round(diameter_ball * 0.625) + "px " + Math.round(diameter_ball < 4 ? 1 : diameter_ball * 0.125) + "px rgba(0, 0, 0, 0.9)"
            });
        })(diameter_ball / 2);
    }
    function nextTask() {
        var max = 100;
        var min = 1;
        $.get("/api/task/get",
            { complexity: Math.floor(Math.random() * (max - min + 1)) + min },
            function(data){
                data = JSON.parse(data);
                applyTask(data.tasks[0]);
            }
        );
    }
    function applyTask(task) {
        /** Отображаем шары */
        var ball1 = $("#ball1");
        var ball2 = $("#ball2");

        ball1.css("top", (task[0][0] * 4) + "%" );
        ball1.css("left", (task[0][1] * 1.96) + "%" );

        ball2.css("top", (task[1][0] * 4) + "%" );
        ball2.css("left", (task[1][1] * 1.96) + "%" );

        //игровой шар
        $("#ball" + (task[4])).css("background", "radial-gradient(circle at 50% 0%, #fffff0, #fffff0 10%, #b9b9ae 80%, #8c8c83 100%)");
        $("#ball" + (task[4] + 2)).css("background", "radial-gradient(circle at 50% 0%, #fffff0, #fffff0 10%, #b9b9ae 80%, #8c8c83 100%)");
        //биток
        $("#ball" + (task[4] + 1)).css("background", "radial-gradient(circle at 50% 0%, #dc0000, #ba0000 10%, #800000 80%, #640000 100%)")
            .click(function() {
                openBallForTarget([0, 0]);
            });

        //помечаем лузу, в которую нужно забить
        $(".pockets").removeClass("active");
        $("#pocket" + task[2]).addClass("active");

        /** Отрисовка траекторий и разметки на поле */
        var board = $(".board");
        var board_height = board.height();
        var board_width = board.width();

        var trajectory_elem = document.getElementById("trajectory");
        trajectory_elem.width = board_width;
        trajectory_elem.height = board_height;
        var trajectory = trajectory_elem.getContext("2d");

        //пунктирная линия
        //trajectory.setLineDash([5,5]);

        /** отрисовка разметки поля */
        var diameter_ball = Math.round($(".table-wrap").width() / 51);

        trajectory.beginPath();
        trajectory.moveTo(board_width / 4 - 0.5, 0);
        trajectory.lineTo(board_width / 4 - 0.5, board_height);
        trajectory.strokeStyle = "yellow";
        trajectory.stroke();

        trajectory.beginPath();
        trajectory.arc(board_width / 4 - 0.5, board_height / 2 - 0.5, diameter_ball / 4, 0, 2 * Math.PI, false);
        trajectory.fillStyle = "yellow";
        trajectory.fill();

        trajectory.beginPath();
        trajectory.moveTo(board_width / 2 - 0.5, 0);
        trajectory.lineTo(board_width / 2 - 0.5, board_height);
        trajectory.strokeStyle = "yellow";
        trajectory.stroke();

        trajectory.beginPath();
        trajectory.arc(board_width / 2 - 0.5, board_height / 2 - 0.5, diameter_ball / 4, 0, 2 * Math.PI, false);
        trajectory.fillStyle = "yellow";
        trajectory.fill();

        trajectory.beginPath();
        trajectory.moveTo(board_width * 0.75 - 0.5, 0);
        trajectory.lineTo(board_width * 0.75 - 0.5, board_height);
        trajectory.strokeStyle = "yellow";
        trajectory.stroke();

        trajectory.beginPath();
        trajectory.arc(board_width * 0.75 - 0.5, board_height / 2 - 0.5, diameter_ball / 4, 0, 2 * Math.PI, false);
        trajectory.fillStyle = "yellow";
        trajectory.fill();

        /** отрисовка траекторий */
        trajectory.beginPath();
        switch(task[2]) {
            case "0":
                trajectory.moveTo(0, 0);
                break;
            case "1":
                trajectory.moveTo(board_width / 2, 0);
                break;
            case "2":
                trajectory.moveTo(board_width, 0);
                break;
            case "3":
                trajectory.moveTo(board_width, board_height);
                break;
            case "4":
                trajectory.moveTo(board_width / 2, board_height);
                break;
            case "5":
                trajectory.moveTo(0, board_height);
                break;
        }
        trajectory.lineTo(
            (task[ task[4] ? 0 : 1 ][1] + 0.5) * 0.0196 * board_width,
            (task[ task[4] ? 0 : 1 ][0] + 0.5) * 0.04 * board_height
        );
        trajectory.strokeStyle = "#000";
        trajectory.stroke();

        trajectory.beginPath();
        trajectory.moveTo(
            (task[ task[4] ? 0 : 1 ][1] + 0.5) * 0.0196 * board_width,
            (task[ task[4] ? 0 : 1 ][0] + 0.5) * 0.04 * board_height
        );
        trajectory.lineTo(
            (task[ task[4] ][1] + 0.5) * 0.0196 * board_width,
            (task[ task[4] ][0] + 0.5) * 0.04 * board_height
        );
        trajectory.strokeStyle = "#000";
        trajectory.stroke();

        //добавляем в форму номер задания
        $("#task_id").val(task[6]);

        var $task = $(".task");
        $task.text("Забейте \"чужой\" шар в подсвеченную лузу");

        //меняем цвет и размеры блока со сложностью
        var $complexity = $("#complexity");
        $complexity.text(task[5]);
        $complexity.css("background-color", "rgb(" + Math.floor(2.55 * task[5]) + ", 0, " + Math.floor(2.55 * (100 - task[5])) + ")")
            .css("line-height", $task.height() + "px")
            .css("min-width", $complexity.height() + "px");

        //уменьшаем блок задания соразмерно блоку со сложностью
        $task.css("margin-right", $complexity.outerWidth());
    }
    function sendResult(result) {
        $.get("/api/task/save",
            { task_id: $("#task_id").val(), result: result }
        );
    }
    function openBallForTarget() {
        var popoverHTML = '<div class="popover ball-for-target remove-on-close">'+
            '<i class="fa fa-close target"></i>' +
            '</div>';

        var board = $(".board");
        var modal =  $(popoverHTML).show()
            .css({
                top: (board.offset().top + board.height() / 2) + "px",
                width: (board.height() / 2) + "px",
                height: (board.height() / 2) + "px"
            });
        $('body').append(modal[0]);
        app.openModal(modal);

        modal.click(function() {
            var $this = $(this);
            app.closeModal($this);
        });
    }
    calcBoard();
    nextTask();
    $(window).resize(function() {
        calcBoard()
    });
    $("#result1, #result3, #result5, #result10").click(function() {
        nextTask();
        sendResult($(this).text());

        return false;
    });
    $("#skip").click(function() {
        nextTask();
    });
});

app.onPageInit('rating', function (page) {
    var rating_list = $(".rating-list ul");
    var template = rating_list.html();
    rating_list.html("");

    var compiledTemplate = Template7.compile(template);

    $.get("/api/rating/get",
        function(data){
            data = JSON.parse(data);
            if(data.result == "success") {
                rating_list.html(compiledTemplate(data));
            }
        }
    );
});

app.onPageInit('dialogs', function (page) {
    var dialogs_list = $(".dialogs-list ul");
    var template = dialogs_list.html();
    dialogs_list.html("");

    var compiledTemplate = Template7.compile(template);

    function getDialogs() {
        $.get("/api/im/get-dialogs",
            function(data){
                data = JSON.parse(data);
                if(data.result == "success") {
                    dialogs_list.html(compiledTemplate(data));
                }
            }
        );
        if(mainView.activePage.name == "dialogs") {
            setTimeout(getDialogs, 5000);
        }
    }
    getDialogs();
});

app.onPageInit('im', function (page) {
    var last_id = 0;
    var messages = app.messages('.messages');
    var message_bar = app.messagebar('.messagebar');

    messages.clean();

    var id = page.query.id;
    function getMessages() {
        $.get("/api/im/get-messages",
            {interlocutor_id: id, last_id: last_id},
            function(data){
                data = JSON.parse(data);
                if(data.result == "success") {
                    if(data.messages.length > 0) {
                        last_id = data.messages[data.messages.length - 1].id;
                        messages.addMessages(data.messages);
                        messages.layout();
                    }
                }
            }
        );
        if(mainView.activePage.name == "im" && mainView.activePage.query.id == id) {
            setTimeout(getMessages, 1000);
        }
    }
    getMessages();

    $('.messagebar .link').on('click', function () {
        // Message text
        var messageText = message_bar.value().trim();
        // Exit if empty message
        if (messageText.length === 0) return;

        $.post("/api/im/send?interlocutor_id=" + id,
            {text: messageText},
            function(data){
                data = JSON.parse(data);
                if(data.result == "success") {
                    message_bar.clear();
                }
            }
        );
    });
});