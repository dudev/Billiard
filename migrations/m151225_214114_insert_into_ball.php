<?php

use yii\db\Schema;
use yii\db\Migration;

class m151225_214114_insert_into_ball extends Migration
{
    public function up()
    {
        set_time_limit(0);
        for($x1 = 0; $x1 <= 24; $x1++) {
            for($y1 = 0; $y1 <= 50; $y1++) {
                $rows = [];
                for($x2 = $x1; $x2 <= 24; $x2++) {
                    for($y2 = $y1; $y2 <= 50; $y2++) {
                        if($x1 != $x2 || $y1 != $y2) {
                            $rows[] = [
                                'x1' => $x1, 'y1' => $y1, 'x2' => $x2, 'y2' => $y2,
                            ];
                        }
                    }
                }
                if($rows) {
                    $this->batchInsert('ball', ['x1', 'y1', 'x2', 'y2'], $rows);
                }
            }
        }
    }

    public function down()
    {
        echo "m151225_214114_insert_into_ball cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
