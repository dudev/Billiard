<?php

use yii\db\Schema;
use yii\db\Migration;

class m160108_132855_create_message_table extends Migration
{
    public function up()
    {
	    $this->createTable('message', [
		    'id' => Schema::TYPE_PK,
		    'owner_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'interlocutor_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'type' => 'ENUM(\'in\', \'out\') NOT NULL',
		    'is_read' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0',
		    'text' => Schema::TYPE_TEXT . ' CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);

	    $this->addForeignKey('owner_id_FK_message', 'message', 'owner_id', 'user', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('interlocutor_id_FK_message', 'message', 'interlocutor_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m160108_132855_create_message_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
