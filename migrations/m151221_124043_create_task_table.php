<?php

use yii\db\Schema;
use yii\db\Migration;

class m151221_124043_create_task_table extends Migration
{
    public function up()
    {
        $this->createTable('task', [
            'id' => Schema::TYPE_PK,
            'balls_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'pocket' => 'ENUM(\'0\', \'1\', \'2\', \'3\', \'4\', \'5\') NOT NULL',
            'score_ball' => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT \'0 - свой, 1 - чужой\'',
            'cue_ball' => Schema::TYPE_SMALLINT . '(1) NOT NULL',
            'complexity' => Schema::TYPE_SMALLINT . '(3) NOT NULL',
        ]);

	    $this->addForeignKey('balls_id_FK_task', 'task', 'balls_id', 'ball', 'id', 'CASCADE', 'CASCADE');
	    $this->createIndex('task_tbl_complexity_idx', 'task', 'complexity');
    }

    public function down()
    {
        echo "m151221_124043_create_task_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
