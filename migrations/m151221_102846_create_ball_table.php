<?php

use yii\db\Schema;
use yii\db\Migration;

class m151221_102846_create_ball_table extends Migration
{
    public function up()
    {
        $this->createTable('ball', [
            'id' => Schema::TYPE_PK,
            'x1' => Schema::TYPE_SMALLINT . ' NOT NULL',
            'y1' => Schema::TYPE_SMALLINT . ' NOT NULL',
            'x2' => Schema::TYPE_SMALLINT . ' NOT NULL',
            'y2' => Schema::TYPE_SMALLINT . ' NOT NULL',
        ]);
    }

    public function down()
    {
        echo "m151221_102846_create_ball_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
