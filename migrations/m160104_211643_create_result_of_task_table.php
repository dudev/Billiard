<?php

use yii\db\Schema;
use yii\db\Migration;

class m160104_211643_create_result_of_task_table extends Migration
{
    public function up()
    {
	    $this->createTable('result_of_task', [
		    'id' => Schema::TYPE_PK,
		    'task_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'result' => Schema::TYPE_INTEGER . ' NOT NULL',
		    'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',
	    ]);

	    $this->addForeignKey('task_id_FK_result_of_task', 'result_of_task', 'task_id', 'task', 'id', 'CASCADE', 'CASCADE');
	    $this->addForeignKey('user_id_FK_result_of_task', 'result_of_task', 'user_id', 'user', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        echo "m160104_211643_create_result_of_task_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
