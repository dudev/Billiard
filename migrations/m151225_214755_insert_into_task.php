<?php

use app\models\Ball;
use app\models\Task;
use yii\db\Query;
use yii\db\Schema;
use yii\db\Migration;

class m151225_214755_insert_into_task extends Migration
{
    public function up()
    {
        set_time_limit(0);
	    ini_set('memory_limit', '1024M');
        $m = (new Query())->from('ball');
        foreach ($m->batch(1000) as $i) {
            $rows = [];
            foreach ($i as $item) {
                //биток - 1
                $pockets = Task::suitable_pockets([$item['x1'], $item['y1']], [$item['x2'], $item['y2']]);
                if($pockets) {
                    foreach ($pockets as $pocket) {
                        $complexity = Task::complexity_of_the_impact([$item['x1'], $item['y1']], [$item['x2'], $item['y2']], $pocket)
	                        * 20 + 0.1;
                        $rows[] = [
                            'balls_id' => $item['id'],
                            'pocket' => (string)$pocket,
                            'score_ball' => 1,
                            'cue_ball' => 0,
                            'complexity' => round($complexity),
                        ];
                    }
                }

                //биток - 2
                $pockets = Task::suitable_pockets([$item['x2'], $item['y2']], [$item['x1'], $item['y1']]);
                if($pockets) {
                    foreach ($pockets as $pocket) {
                        $complexity = Task::complexity_of_the_impact([$item['x2'], $item['y2']], [$item['x1'], $item['y1']], $pocket)
	                        * 20 + 0.1;
                        $rows[] = [
                            'balls_id' => $item['id'],
                            'pocket' => (string)$pocket,
                            'score_ball' => 1,
                            'cue_ball' => 1,
                            'complexity' => round($complexity),
                        ];
                    }
                }
            }
            $this->batchInsert('task', ['balls_id', 'pocket', 'score_ball', 'cue_ball', 'complexity'], $rows);
            unset($q);
            unset($rows);
        }
    }

    public function down()
    {
        echo "m151225_214755_insert_into_task cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
