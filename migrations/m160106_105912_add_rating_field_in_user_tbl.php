<?php

use yii\db\Schema;
use yii\db\Migration;

class m160106_105912_add_rating_field_in_user_tbl extends Migration
{
    public function up()
    {
	    $this->addColumn('user', 'rating', Schema::TYPE_INTEGER);
    }

    public function down()
    {
        echo "m160106_105912_add_rating_field_in_user_tbl cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
