<?php

namespace app\controllers\cabinet;

use app\extensions\Controller;
use app\models\AuthKey;
use app\models\User;
use general\ext\api\auth\AuthApi;
use general\ext\api\auth\AuthUrlCreator;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;

class TrainingController extends Controller {
    //public $defaultAction = 'login';
	public $layout = 'cabinet';
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['signup', 'index'],
				'rules' => [
					[
						'actions' => ['signup'],
						'allow' => true,
						'roles' => ['?'],
					],
					[
						'actions' => ['index'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
    public function actionIndex() {
        return $this->render('index');
    }

	public function actionRun ()
	{
		return $this->render('run');
	}
}