<?php
/**
 * Project: Auth - Seven lights
 * User: Evgeny Rusanov
 * E-mail: admin@dudev.ru
 * Site: dudev.ru
 */

namespace app\controllers\api;


class AuthController extends \general\controllers\api\AuthController {
	public $layout = 'empty';
}