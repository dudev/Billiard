<?php

namespace app\controllers\api;

use app\extensions\ApiController;
use app\models\ResultOfTask;
use app\models\Task;
use yii\base\Exception;
use yii\db\Query;
use yii\filters\AccessControl;

class TaskController extends ApiController {
    protected $_safe_actions = ['get', 'save'];
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['get', 'save'],
				'rules' => [
					[
						'actions' => ['get', 'save'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
    public function actionGet($complexity = 0) {
	    if($complexity < 0 || $complexity > 100) {
		    return $this->sendError(0);
	    }

	    //if(!$complexity) {

	    //}

        /** @var Task $model */
        $model = Task::find()
	        ->where(['complexity' => $complexity])
            ->with('balls')
            ->orderBy('RAND()')
            ->limit(1)
            ->one();

        $task = [
            [$model->balls->x1, $model->balls->y1],
            [$model->balls->x2, $model->balls->y2], //шары
            $model->pocket, //луза
            $model->score_ball, //свой\чужой
            $model->cue_ball, //биток
            round($model->complexity),
	        $model->id,
        ];

        return $this->sendSuccess([
            'tasks' => [
                $task
            ]
        ]);
    }
	public function actionSave($task_id, $result) {
		$transaction = \Yii::$app->getDb()->beginTransaction();

		try {
			$model = new ResultOfTask();
			$model->task_id = $task_id;
			$model->result = $result;
			$model->user_id = \Yii::$app->user->id;
			$model->save();

			$models = (new Query())
				->from('result_of_task')
				->select(['1 / AVG(result_of_task.result) * task.complexity as rating'])
				->where(['>', 'created_at', time() - 30 * 24 * 3600])
				->innerJoin('task', 'result_of_task.task_id = task.id')
				->limit(100)
				->orderBy(['created_at' => SORT_DESC])
				->groupBy('task.complexity')
				->all();

			$rating = 0;
			foreach ($models as $i) {
				$rating += $i['rating'];
			}

			/** @var \app\models\User $user */
			$user = \Yii::$app->user->identity;
			$user->rating = $rating;
			$user->save();

			$transaction->commit();
			return $this->sendSuccess([]);
		} catch(\Exception $e) {
			$transaction->rollBack();
		}

		return $this->sendError(ApiController::ERROR_DB);
	}
}