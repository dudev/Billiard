<?php

namespace app\controllers;

use app\extensions\Controller;
use app\models\AuthKey;
use app\models\User;
use general\ext\api\auth\AuthApi;
use general\ext\api\auth\AuthUrlCreator;
use Yii;
use yii\filters\AccessControl;
use yii\web\Response;

class SiteController extends Controller {
    //public $defaultAction = 'login';
	//public $layout = 'empty';
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only' => ['index', 'rating', 'dialogs', 'logout', 'logout-everywhere'],
				'rules' => [
					[
						'actions' => ['index', 'rating', 'dialogs', 'logout', 'logout-everywhere'],
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }
	public function actionIndex() {
		$this->layout = 'empty';
		return $this->render('index');
	}
	/*public function actionRating() {
		return $this->render('rating');
	}
	public function actionDialogs() {
		return $this->render('dialogs');
	}*/

	/*public function actionSignup() {
		$model = new SignupForm();
		if ($model->load(Yii::$app->request->post())) {
			if ($user = $model->signup()) {
				if (Yii::$app->user->login($user, Yii::$app->params['durationAuth'])) {
					return $this->goHome();
				}
			}
		}

		return $this->render('signup', [
			'model' => $model,
		]);
	}*/

	public function actionSignup() {
		$this->layout = 'emptyHtml';
		return $this->render('signup');
	}

    public function actionLoginWithAuthKey($result) {
	    $this->layout = 'emptyHtml';
	    if($result == 'success') {
		    if (!Yii::$app->user->isGuest) {
			    return $this->render('redirect', ['url' => '/']);
		    }

		    $mark = Yii::$app->request->get('key');
		    if($key = AuthApi::userGetServiceAuthKey($mark)) {
			    $user = User::getFromAuth($key['user_id']);

			    $model = new AuthKey();
			    $model->auth_key = $key['auth_key'];
			    $model->browser = Yii::$app->request->userAgent;
			    $model->ip = Yii::$app->request->userIP;
			    $model->user_id = $key['user_id'];
			    $model->save();

			    if (Yii::$app->user->login($user, Yii::$app->params['durationAuth'])) {
				    return $this->render('refresh');
			    }
		    } else {
			    //@todo сделать вывод ошибки
		    }
	    }
	    return $this->render('redirect', ['url' => '/']);
    }
	public function actionLoginWithMark($mark) {
		if (!Yii::$app->user->isGuest) {
			return '';
		}

		\Yii::$app->response->format = Response::FORMAT_JSONP;
		if($key = AuthApi::userGetServiceAuthKey($mark)) {
			$user = User::getFromAuth($key['user_id']);

			$model = new AuthKey();
			$model->auth_key = $key['auth_key'];
			$model->browser = Yii::$app->request->userAgent;
			$model->ip = Yii::$app->request->userIP;
			$model->user_id = $key['user_id'];
			$model->save();

			if (Yii::$app->user->login($user, Yii::$app->params['durationAuth'])) {
				return [
					'data' => 'success',
					'callback' => 'loginWithMark',
				];
			}
		}
		return [
			'data' => 'error',
			'callback' => 'loginWithMark',
		];
	}
	public function actionLogin() {
		if (!Yii::$app->user->isGuest) {
			return $this->goHome();
		}
		$this->layout = 'emptyHtml';
		return $this->render('login');
	}
    public function actionLogout() {
        User::logout();
	    return $this->redirect(
		    AuthUrlCreator::userLogout(
			    Yii::$app->id,
			    '//' . Yii::$app->request->getServerName()));
    }
	public function actionLogoutEverywhere() {
		User::logoutEverywhere();
		return $this->redirect(
			AuthUrlCreator::userLogoutEverywhere(
				Yii::$app->id,
				'//' . Yii::$app->request->getServerName()));
	}
}