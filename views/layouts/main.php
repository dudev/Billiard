<?php
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $content string */

$this->registerJsFile('//code.jquery.com/jquery-1.11.1.min.js', ['position' => \yii\web\View::POS_HEAD]);
$this->registerCssFile('/css/font-awesome.min.css');

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
	<style>
		body {
			margin: 0;
			font-family: Arial, Helvetica, Verdana, Tahoma, sans-serif;
		}
		.header {
			height: 2em;
			background-color: #333;
		}
		.header a, .header div {
			display: block;
			margin: .5em;
			color: white;
			text-decoration: none;
		}
		#sandwich, #title {
			float: left;
		}
		#envelope {
			float: right;
		}
		#menu {
			z-index: 10;
			position: absolute;
			background-color: #555;
			margin: 0;
			padding: 20px 10px;
			display: none;
		}
		#menu a {
			color: #eee;
			text-decoration: none;
			display: block;
			padding: 10px 20px;
			border-bottom: 1px solid rgba(238, 238, 238, .4);
		}
		#menu a:last-child {
			border: none;
		}
		#menu a i {
			padding-right: 15px;
		}
		#back-menu {
			height: 100%;
			width: 100%;
			position: absolute;
			margin: 0;
			opacity: 0.7;
			background-color: #000;
			display: none;
			z-index: 9;
		}
	</style>
</head>
<body>
<?php $this->beginBody() ?>
<div id="menu">
	<a href="<?= Yii::$app->urlManager->createUrl(['site/index']) ?>"><i class="fa fa-flag"></i>Тренировка</a>
	<a href="<?= Yii::$app->urlManager->createUrl(['site/rating']) ?>"><i class="fa fa-bar-chart"></i>Рейтинг</a>
	<a href="<?= Yii::$app->urlManager->createUrl(['site/dialogs']) ?>"><i class="fa fa-envelope-o"></i>Сообщения</a>
</div>
<div id="back-menu"></div>

<div class="header">
	<a class="fa fa-reorder" id="sandwich" href="#"></a>
	<div id="title"><?= Html::encode($this->title) ?></div>
	<a href="<?= Yii::$app->urlManager->createUrl(['site/dialogs']) ?>" class="fa fa-envelope-o" id="envelope"></a>
</div>
<script>
	$(document).ready(function(){
		$("#sandwich").click(showMenu);
		drawMenu();
	});
	function showMenu() {
		$("#menu").show();
		$("#back-menu")
			.show()
			.click(hideMenu);
	}
	function hideMenu() {
		$("#menu").hide();
		$("#back-menu")
			.hide()
			.unbind();
	}
	function drawMenu() {
		var menu = $("#menu");
		menu.css("min-height", "calc(100% - 40px)");
	}
</script>
<?= $content ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
