<?php
/* @var $this yii\web\View */
use app\models\User;
use yii\data\ActiveDataProvider;
use yii\grid\GridView;

$this->title = 'Рейтинг';

$dataProvider = new ActiveDataProvider([
	'query' => User::find(),
	'pagination' => [
		'pageSize' => 20,
	],
]);

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'layout' => "{items}\n{pager}",
	'columns' => [
		'nick',
		'rating',
		[
			'class' => 'yii\grid\DataColumn', // can be omitted, as it is the default
			'value' => function ($data) {
				return '<a href="' . Yii::$app->urlManager->createUrl(['message/dialog', 'id' => $data->id]) . '">ЛС</a>'; // $data['name'] for array data, e.g. using SqlDataProvider.
			},
			'format' => 'raw',
		],
	],
]);
?>
<style>
	.table {
		width: 100%;
		text-align: center;
	}
</style>
