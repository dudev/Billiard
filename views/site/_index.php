<?php
/* @var $this yii\web\View */
$this->title = 'Тренировка';

?>
<style>
	.marks {
		width: 100%;
		margin: 0;
	}
	#mark input {
		position: relative;
		z-index: 2;
	}
	#mark input, #mark button {
		font-size: 1.1em;
		padding: .1em;
		/* width: 100%; */
		box-sizing: border-box;
	}
	#mark button {
		/* width: 50%; */
		float: left;
	}
	.board {
		width: 100%;
		background-color: #0f610f;
		position: relative;
	}
	.task {
		background-color: #dddddd;
		padding: 0.5em;
		z-index: 2;
		margin-right: 2em;
		position: relative;
	}
	.pockets {
		width: 1.96%;
		height: 4%;
		position: absolute;
		background-color: gray;
		z-index: 1;
	}
	#pocket0 {
		width: 1.15%;
		height: 2.35%;
		border-radius: 0 0 100% 0;
	}
	#pocket1 {
		left: 49.02%;
		border-radius: 0 0 50% 50%;
		top: -2%;
	}
	#pocket2 {
		width: 1.15%;
		height: 2.35%;
		border-radius: 0 0 0 100%;
		left: 98.85%;
	}
	#pocket3 {
		width: 1.15%;
		height: 2.35%;
		border-radius: 100% 0 0 0;
		left: 98.85%;
		top: 97.65%
	}
	#pocket4 {
		left: 49.02%;
		border-radius: 50% 50% 0 0;
		top: 98%
	}
	#pocket5 {
		width: 1.15%;
		height: 2.35%;
		border-radius: 0 100% 0 0;
		top: 97.65%
	}
	.pockets.active {
		background-color: yellow;
	}
	#complexity {
		color: white;
		text-align: center;
		padding: 0.5em;
		float: right;
		font-weight: bold;
	}

	.balls {
		width: 1.96%;
		height: 4%;
		position: absolute;
		z-index: 2;
		border-radius: 50%;
	}
</style>
<div>
	<div id="complexity"></div>
	<div class="task"></div>
</div>
<div class="board">
	<div class="balls" id="ball1"></div>
	<div class="balls" id="ball2"></div>

    <div id="pocket0" class="pockets"></div>
    <div id="pocket1" class="pockets"></div>
    <div id="pocket2" class="pockets"></div>
    <div id="pocket3" class="pockets"></div>
    <div id="pocket4" class="pockets"></div>
    <div id="pocket5" class="pockets"></div>
</div>
<div class="marks">
    <form id="mark">
	    <input type="hidden" value="" id="task_id">
	    <button id="result1" type="button">1</button>
	    <button id="result3" type="button">3</button>
	    <button id="result5" type="button">5</button>
	    <button id="result10" type="button">10</button>
	    <!--input type="number" id="result" value="" min="1" placeholder="Номер удачной попытки"-->
        <button id="skip" type="button">Пропустить</button>
        <!--button id="enter" type="submit">OK</button-->
	    <style>
		    #skip {
			    width: 100%;
		    }
		    #result1, #result3, #result5, #result10 {
			    width: 25%;
			    position: relative;
			    z-index: 2;
		    }
	    </style>
    </form>
</div>




<script>
    $(document).ready(function(){
        nextTask();
        calcBoard()
    });
    /* $("#mark").submit(function() {
        nextTask();
        $(this).find("input").blur();

	    $.get("/api/task/save",
		    { task_id: $("#task_id").val(), result: $("#result").val() },
		    function(){
			    $("#result").val("");
		    }
	    );

        return false;
    }); */
    $("#result1, #result3, #result5, #result10").click(function() {
	    nextTask();
	    sendResult($(this).text());

	    return false;
    });
    function sendResult(result) {
	    $.get("/api/task/save",
		    { task_id: $("#task_id").val(), result: result }
	    );
    }
    $("#skip").click(function() {
        nextTask();
    });
    $(window).resize(function() {
        calcBoard()
    });

    function applyTask(task) {
        var ball1 = $("#ball1");
        var ball2 = $("#ball2");

        ball1.css("top", (task[0][0] * 4) + "%" );
        ball1.css("left", (task[0][1] * 1.96) + "%" );

        ball2.css("top", (task[1][0] * 4) + "%" );
        ball2.css("left", (task[1][1] * 1.96) + "%" );

	    //игровой шар
        $("#ball" + (task[4])).css("background", "radial-gradient(circle at 50% 0%, #fffff0, #fffff0 10%, #b9b9ae 80%, #8c8c83 100%)");
        $("#ball" + (task[4] + 2)).css("background", "radial-gradient(circle at 50% 0%, #fffff0, #fffff0 10%, #b9b9ae 80%, #8c8c83 100%)");
	    //биток
        $("#ball" + (task[4] + 1)).css("background", "radial-gradient(circle at 50% 0%, #ff0000, #ef0000 10%, #cc0000 80%, #9c0000 100%)");

        $(".pockets").removeClass("active");
	    $("#pocket" + task[2]).addClass("active");

	    $("#task_id").val(task[6]);

	    var $task = $(".task");
	    var $complexity = $("#complexity");

	    $task.text("Забейте \"чужой\" шар в подсвеченную лузу");

	    $complexity.text(task[5])
		    .css("background-color", "rgb(" + Math.floor(2.55 * task[5]) + ", 0, " + Math.floor(2.55 * (100 - task[5])) + ")")
		    .css("line-height", $task.height() + "px")
		    .css("min-width", $complexity.height() + "px");

	    $task.css("margin-right", $complexity.outerWidth());
    }
    function nextTask() {
	    var max = 100;
	    var min = 1;
        $.get("/api/task/get",
            { complexity: Math.floor(Math.random() * (max - min + 1)) + min },
            function(data){
                applyTask(data.tasks[0]);
            }
        );
    }
    function calcBoard() {
        var board = $(".board");
        board.height(board.width() / 2);

	    drawMenu();
    }
</script>