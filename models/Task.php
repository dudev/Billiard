<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property integer $balls_id
 * @property string $pocket
 * @property integer $score_ball
 * @property integer $cue_ball
 * @property double $complexity
 *
 * @property Ball $balls
 */
class Task extends \yii\db\ActiveRecord
{
    public static $pockets = [
        [-0.5, -0.5],
        [-0.5, 25],
        [-0.5, 50.5],
        [24.5, 50.5],
        [24.5, 25],
        [24.5, -0.5],
    ];
    public static $pockets_line_dot = [
        [1, 1],
        [1, 25],
        [1, 49],
        [23, 49],
        [23, 25],
        [23, 1],
    ];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['balls_id', 'pocket', 'score_ball', 'cue_ball', 'complexity'], 'required'],
            [['balls_id', 'score_ball', 'cue_ball'], 'integer'],
            [['pocket'], 'string'],
            [['complexity'], 'number']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'balls_id' => 'Balls ID',
            'pocket' => 'Pocket',
            'score_ball' => '0 - свой, 1 - чужой',
            'cue_ball' => 'Cue Ball',
            'complexity' => 'Complexity',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBalls()
    {
        return $this->hasOne(Ball::className(), ['id' => 'balls_id']);
    }

    public static function suitable_pockets(array $ball1, array $ball2)
    {
        //массив подходящих луз
        $arr_yes = [];

        //координаты шаров
        $x1 = $ball1[0];
        $y1 = $ball1[1];
        $x2 = $ball2[0];
        $y2 = $ball2[1];

        if ($y1 == $y2) { //если перпендикулярная лини будет параллельна длинному борту и оси Oy
            if ($x1 < $x2) {
                $arr_yes = [3, 4, 5];
            } else {
                $arr_yes = [0, 1, 2];
            }
        } else {
            //найдем уравнение прямой перпендикулярной данной
            $k = -($x2 - $x1) / ($y2 - $y1);
            $b = $x2 * ($x2 - $x1) / ($y2 - $y1) + $y2;

            //var_dump($k, $b);

            if ($k * $x1 + $b >= $y1) {
                //<
                foreach (self::$pockets as $key => $pocket) {
                    if ($k * $pocket[0] + $b < $pocket[1]) {
                        $arr_yes[] = $key;
                    }
                }
            } else {
                //>
                foreach (self::$pockets as $key => $pocket) {
                    //var_dump($k * $pocket[0] + $b >= $pocket[1], $pocket);
                    if ($k * $pocket[0] + $b > $pocket[1]) {
                        $arr_yes[] = $key;
                    }
                }
            }
        }

        foreach ($arr_yes as $k => $i) {
            if(($x2 - $x1) && (self::$pockets[$i][0] - $x2)) {
                //прямая через шары
                $kb = ($y2 - $y1) / ($x2 - $x1);
                //прямая через 2-й шар и лузу
                $kp = (self::$pockets[$i][1] - $y2) / (self::$pockets[$i][0] - $x2);

                //отклонение от первоначальной траектории движения шара
                if(!(1 + $kb * $kp)) {
                    unset($arr_yes[$k]);
                    continue;
                }
                $tg = abs(($kb - $kp) / (1 + $kb * $kp));
                if ($tg > 4) { //принимаем только те случаи, когда траектория изменяется не более, чем на 75 градусов
                    unset($arr_yes[$k]);
                    continue;
                }

                if($i == 1 || $i == 4) {
                    $kl = (self::$pockets[$i][1] - static::$pockets_line_dot[$i][1]) / (self::$pockets[$i][0] - static::$pockets_line_dot[$i][0]);

                    if(!(1 + $kl * $kp)) {
                        unset($arr_yes[$k]);
                        continue;
                    }
                    $tg = abs(($kl - $kp) / (1 + $kl * $kp));
                    if ($tg > 1) { //принимаем только те случаи, когда шар входит в лузу с углом отклонения не более 45 градусов
                        unset($arr_yes[$k]);
                    }
                }
            } elseif($x2 - $x1) {
                //прямая через шары
                $kb = ($y2 - $y1) / ($x2 - $x1);

                //отклонение от первоначальной траектории движения шара
                if(!$kb) {
                    unset($arr_yes[$k]);
                    continue;
                }
                $tg = abs(1 / $kb);
                if ($tg > 4) { //принимаем только те случаи, когда траектория изменяется не более, чем на 75 градусов
                    unset($arr_yes[$k]);
                    continue;
                }

                //отклонение от центральной прямой лузы
                if($i == 1 || $i == 4) {
                    $kl = (self::$pockets[$i][1] - static::$pockets_line_dot[$i][1]) / (self::$pockets[$i][0] - static::$pockets_line_dot[$i][0]);

                    if(!$kl) {
                        unset($arr_yes[$k]);
                        continue;
                    }
                    $tg = abs(1 / $kl);
                    if ($tg > 1) { //принимаем только те случаи, когда шар входит в лузу с углом отклонения не более 45 градусов
                        unset($arr_yes[$k]);
                    }
                }
            } else {
                //прямая через 2-й шар и лузу
                $kp = (self::$pockets[$i][1] - $y2) / (self::$pockets[$i][0] - $x2);

                //отклонение от первоначальной траектории движения шара
                if(!$kp) {
                    unset($arr_yes[$k]);
                    continue;
                }
                $tg = abs(1 / $kp);
                if ($tg > 4) { //принимаем только те случаи, когда траектория изменяется не более, чем на 75 градусов
                    unset($arr_yes[$k]);
                    continue;
                }

                //отклонение от центральной прямой лузы
                if($i == 1 || $i == 4) {
                    $kl = (self::$pockets[$i][1] - static::$pockets_line_dot[$i][1]) / (self::$pockets[$i][0] - static::$pockets_line_dot[$i][0]);

                    if(!(1 + $kl * $kp)) {
                        unset($arr_yes[$k]);
                        continue;
                    }
                    $tg = abs(($kl - $kp) / (1 + $kl * $kp));
                    if ($tg > 1) { //принимаем только те случаи, когда шар входит в лузу с углом отклонения не более 45 градусов
                        unset($arr_yes[$k]);
                    }
                }
            }
        }

        return $arr_yes;
    }
    public static function complexity_of_the_impact (array $ball1, array $ball2, $pocket)
    {
        $xp = Task::$pockets[$pocket][0];
        $yp = Task::$pockets[$pocket][1];
        $x1 = $ball1[0];
        $y1 = $ball1[1];
        $x2 = $ball2[0];
        $y2 = $ball2[1];

        //постоянные коэффициенты для калибровки
        $coefficients = [
            1,
            1,
            1,
            1,
        ];

        //расстояние, после которого множитель начинает расти в обратную сторону
        $critical_distance = 3;

        //прямая из центра лузы
        $kl = ($yp - static::$pockets_line_dot[ $pocket ][1]) / ($xp - static::$pockets_line_dot[ $pocket ][0]);

        //находим отклонение от первоначальной траектории движения шара и отклонение от центральной прямой лузы
        if(($x2 - $x1) && ($xp - $x2)) {
            //прямая через шары
            $kb = ($y2 - $y1) / ($x2 - $x1);
            //прямая через 2-й шар и лузу
            $kp = ($yp - $y2) / ($xp - $x2);

            //отклонение от первоначальной траектории движения шара
            $tg_balls = ($kb - $kp) / (1 + $kb * $kp);

            //отклонение от центральной прямой лузы
            $tg_pocket = ($kp - $kl) / (1 + $kp * $kl);
        } elseif($x2 - $x1) {
            //прямая через шары
            $kb = ($y2 - $y1) / ($x2 - $x1);

            //отклонение от первоначальной траектории движения шара
            $tg_balls = 1 / $kb;

            //отклонение от центральной прямой лузы
            $tg_pocket = 1 / $kl;
        } else {
            //прямая через 2-й шар и лузу
            $kp = ($yp - $y2) / ($xp - $x2);

            //отклонение от первоначальной траектории движения шара
            $tg_balls = 1 / $kp;

            //отклонение от центральной прямой лузы
            $tg_pocket = ($kp - $kl) / (1 + $kp * $kl);
        }

        //расстояние между шарами
        $sb = sqrt(pow($x2 - $x1, 2) + pow($y2 - $y1, 2));

        //расстояние между 2-м шаром и лузой
        $sp = sqrt(pow($xp - $x2, 2) + pow($yp - $y2, 2));

        $multiplier_tg_balls = 1 + abs($tg_balls);

        $multiplier_tg_pocket = 1 + abs($tg_pocket);

        if($sb >= $critical_distance) {
            $multiplier_sb = $sb / 37;//sqrt(pow(24, 2) + pow(50, 2));
        } else {
            $multiplier_sb = pow($critical_distance, $critical_distance + 1 - $sb) / 37;//pow($critical_distance, $critical_distance + 1);
        }
        $multiplier_sb += 0.92;

        if($sp >= $critical_distance) {
            $multiplier_sp = $sp / 37;//sqrt(pow(24, 2) + pow(20, 2));
        } else {
            $multiplier_sp = pow($critical_distance, $critical_distance + 1 - $sp) / 37;//pow($critical_distance, $critical_distance + 1);
        }
        $multiplier_sp += 0.92;

        $res = $coefficients[0] * $multiplier_sb
            * $coefficients[1] * $multiplier_sp
            * $coefficients[2] * $multiplier_tg_balls
            * $coefficients[3] * $multiplier_tg_pocket;

	    $res = log($res, 2);

        /* print_r($multiplier_sb);
        print_r($multiplier_sp);
        print_r($multiplier_tg_balls);
        print_r($multiplier_tg_pocket); */

        return $res;
    }
    private function generator() {
        do {
            $ball1 = [
                rand(0, 24),
                rand(0, 50),
            ];
            $ball2 = [
                rand(0, 24),
                rand(0, 50),
            ];
            $pockets = Task::suitable_pockets($ball1, $ball2);
        } while(empty($pockets));
        return [
            $ball1,
            $ball2, //шары
            $pockets[ array_rand($pockets) ], //луза
            1, //свой\чужой
            0, //биток
        ];
    }
}
