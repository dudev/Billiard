<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\BaseActiveRecord;

/**
 * This is the model class for table "message".
 *
 * @property integer $id
 * @property integer $owner_id
 * @property integer $interlocutor_id
 * @property string $type
 * @property integer $is_read
 * @property string $text
 * @property integer $create_at
 *
 * @property User $interlocutor
 * @property User $owner
 */
class Message extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'message';
    }

	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					BaseActiveRecord::EVENT_BEFORE_INSERT => 'created_at',
				],
			],
		];
	}

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'interlocutor_id', 'type', 'text'], 'required'],
            [['owner_id', 'interlocutor_id'], 'integer'],
	        ['owner_id', 'exist', 'targetClass' => User::className(), 'targetAttribute' => 'id'],
	        ['interlocutor_id', 'exist', 'targetClass' => User::className(), 'targetAttribute' => 'id'],
            [['type', 'text'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'owner_id' => 'Owner ID',
            'interlocutor_id' => 'Interlocutor ID',
            'type' => 'Type',
            'is_read' => 'Is Read',
            'text' => 'Text',
            'created_at' => 'Create At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInterlocutor()
    {
        return $this->hasOne(User::className(), ['id' => 'interlocutor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwner()
    {
        return $this->hasOne(User::className(), ['id' => 'owner_id']);
    }
}
