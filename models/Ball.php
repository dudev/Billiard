<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ball".
 *
 * @property integer $id
 * @property integer $x1
 * @property integer $y1
 * @property integer $x2
 * @property integer $y2
 */
class Ball extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ball';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['x1', 'y1', 'x2', 'y2'], 'required'],
            [['x1', 'y1', 'x2', 'y2'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'x1' => 'X1',
            'y1' => 'Y1',
            'x2' => 'X2',
            'y2' => 'Y2',
        ];
    }
}
